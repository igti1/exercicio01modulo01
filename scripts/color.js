window.addEventListener('load', loadSecure);

function loadSecure() {
  var color1Input = document.getElementById('color1');
  var color2Input = document.getElementById('color2');
  var color3Input = document.getElementById('color3');

  color1Input.addEventListener('input', changeColor);
  color2Input.addEventListener('input', changeColor);
  color3Input.addEventListener('input', changeColor);
}

function changeColor() {
  console.log('veio aqui');
  var color1 = document.getElementById('color1').value;
  var color2 = document.getElementById('color2').value;
  var color3 = document.getElementById('color3').value;

  document.getElementById('colorPallet').style.backgroundColor =
    'rgb(' + color1 + ', ' + color2 + ', ' + color3 + ')';
  changeColorOutput(color1, color2, color3);
}

function changeColorOutput(color1, color2, color3) {
  document.getElementById('color1Output').value = color1;
  document.getElementById('color2Output').value = color2;
  document.getElementById('color3Output').value = color3;
}
